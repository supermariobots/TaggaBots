# syntax=docker/dockerfile:1

# Comments are provided throughout this file to help you get started.
# If you need more help, visit the Dockerfile reference guide at
# https://docs.docker.com/engine/reference/builder/

ARG PYTHON_VERSION=3.12
FROM docker.io/python:${PYTHON_VERSION}-alpine AS base

# Install pipenv
RUN pip install pipenv

# Prevents Python from writing pyc files.
ENV PYTHONDONTWRITEBYTECODE=1

# Keeps Python from buffering stdout and stderr to avoid situations where
# the application crashes without emitting any logs due to buffering.
ENV PYTHONUNBUFFERED=1

# Create a non-privileged user that the app will run under.
# See https://docs.docker.com/go/dockerfile-user-best-practices/
ARG UID=10001
RUN adduser \
    --disabled-password \
    --gecos "" \
    --home "/app" \
    --shell "/sbin/nologin" \
    --no-create-home \
    --uid "${UID}" \
    app


FROM base AS builder
RUN apk add --no-cache alpine-sdk cargo

# Switch to the non-privileged user to build the application.
USER app
WORKDIR /app

# Build .venv with pipenv
RUN --mount=type=bind,source=Pipfile,target=Pipfile \
    --mount=type=bind,source=Pipfile.lock,target=Pipfile.lock \
    PIPENV_VENV_IN_PROJECT=1 pipenv install --deploy


FROM base as runner

# Switch to the non-privileged user to run the application.
USER app
WORKDIR /app

# Copy .venv from builder
COPY --from=builder /app/.venv /app/.venv

# Copy the source code into the container.
COPY --chown=app:app . .

# Run the application.
CMD [ "/usr/bin/env", "PIPENV_VENV_IN_PROJECT=1", "/usr/local/bin/pipenv", "run", "python", "./TaggaBots.py" ]
