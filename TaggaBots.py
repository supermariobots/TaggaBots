#!/usr/bin/env -S uv run python
# vim: cc=88 tw=88 ts=4 sts=4 sw=4 et

# SPDX-FileCopyrightText: 2023 Mario-ttide
# SPDX-License-Identifier: AGPL-3.0-or-later


import asyncio
import html
import random
import re

from arq import create_pool
from arq.connections import RedisSettings
from telethon import events
from telethon.errors.rpcerrorlist import ChatWriteForbiddenError
from telethon.tl.functions.channels import GetParticipantRequest
from telethon.types import (
    BotCommand,
    BotCommandScopeChatAdmins,
    BotCommandScopeChats,
    BotCommandScopeUsers,
    Channel,
    ChannelParticipantBanned,
)
from telethon.utils import get_display_name, get_peer_id

from common import BotClone, main

ENTITIES_PER_MESSAGE = 5


class TaggaBots(BotClone):
    @classmethod
    async def before_create(cls, pool):
        cls.arq = await create_pool(
            RedisSettings(unix_socket_path="/run/valkey/valkey.sock"),
            default_queue_name="arq:queue:TaggaBots",
        )

    async def start(self, *args, **kwargs):
        self.worker_tasks = set()

        self.add_event_handler(
            self.onlyadmins_hndlr,
            events.NewMessage(
                incoming=True,
                pattern=rf"(?i)/onlyadmins(@{self.username})?$",
                func=lambda e: not e.is_private,
            ),
        )
        self.add_event_handler(
            self.noonlyadmins_hndlr,
            events.NewMessage(
                incoming=True,
                pattern=rf"(?i)/noonlyadmins(@{self.username})?$",
                func=lambda e: not e.is_private,
            ),
        )
        self.add_event_handler(
            self.all_hndlr,
            events.NewMessage(
                incoming=True,
                pattern=rf"(?i)/all(@{self.username})?",
                func=lambda e: not e.is_private,
            ),
        )
        self.add_event_handler(
            self.all_hndlr,
            events.NewMessage(
                incoming=True,
                pattern=re.compile(r"(?i)[#@]all(\s.*)?", re.DOTALL),
                func=lambda e: not e.is_private,
            ),
        )
        self.add_event_handler(
            self.stopall_hndlr,
            events.NewMessage(
                incoming=True,
                pattern=rf"(?i)^/stopall(@{self.username})?$",
                func=lambda e: not e.is_private,
            ),
        )
        self.add_event_handler(
            self.copymessage_hndlr,
            events.NewMessage(
                incoming=True,
                pattern=rf"(?i)^/copymessage(@{self.username})?$",
                func=lambda e: not e.is_private,
            ),
        )
        self.add_event_handler(
            self.nocopymessage_hndlr,
            events.NewMessage(
                incoming=True,
                pattern=rf"(?i)^/nocopymessage(@{self.username})?$",
                func=lambda e: not e.is_private,
            ),
        )
        self.add_event_handler(
            self.vacation_hndlr,
            events.NewMessage(
                incoming=True,
                pattern=rf"(?i)^/vacation(@{self.username})?(\s+@?(?P<chat_id>-\d+))?$",
                func=lambda e: e.is_private,
            ),
        )
        self.add_event_handler(
            self.stopvacation_hndlr,
            events.NewMessage(
                incoming=True,
                pattern=(
                    rf"(?i)^/stopvacation(@{self.username})?(\s+@?(?P<chat_id>-\d+))?$"
                ),
                func=lambda e: e.is_private,
            ),
        )
        self.add_event_handler(
            self.vacations_hndlr,
            events.NewMessage(
                incoming=True,
                pattern=rf"(?i)^/vacations(@{self.username})?$",
                func=lambda e: e.is_private,
            ),
        )
        self.add_event_handler(
            self.lottery_hndlr,
            events.NewMessage(
                incoming=True,
                pattern=rf"(?i)^/lottery(@{self.username})?$",
                func=lambda e: not e.is_private,
            ),
        )
        self.add_event_handler(
            self.start_hndlr,
            events.NewMessage(
                incoming=True,
                pattern=r"(?i)^/start$",
                func=lambda e: e.is_private,
            ),
        )
        self.add_event_handler(
            self.start_hndlr,
            events.NewMessage(
                incoming=True,
                pattern=r"(?i)^/help$",
                func=lambda e: e.is_private,
            ),
        )
        self.add_event_handler(self.deleted_hndlr, events.MessageDeleted())

        await super().start(*args, **kwargs)

        await self.process_queues()

        await self.set_bot_commands(
            [
                BotCommand("start", "Avvia il bot"),
                BotCommand("clone", "Clona il bot"),
            ],
            scope=BotCommandScopeUsers(),
            lang_code="it",
        )
        await self.set_bot_commands(
            [
                BotCommand("all", "Tagga tutti i membri"),
                BotCommand("stopall", "Ferma @all"),
                BotCommand("lottery", "Estrai, e tagga, un utente dal gruppo"),
            ],
            scope=BotCommandScopeChats(),
            lang_code="it",
        )
        await self.set_bot_commands(
            [
                BotCommand("all", "Tagga tutti i membri"),
                BotCommand("stopall", "Ferma @all"),
                BotCommand(
                    "copymessage",
                    "Imposta il bot per copiare il testo del messaggio"
                    " contentente @all invece di rispondere ad esso",
                ),
                BotCommand(
                    "nocopymessage",
                    "Imposta il bot per rispondere al messaggio contenente @all",
                ),
                BotCommand(
                    "onlyadmins", "Imposta il bot per funzionare solo con gli admin"
                ),
                BotCommand("noonlyadmins", "Imposta il bot per funzionare con tutti"),
            ],
            scope=BotCommandScopeChatAdmins(),
            lang_code="it",
        )
        await self.set_bot_commands(
            [
                BotCommand("start", "Start the bot"),
                BotCommand("clone", "Clone the bot"),
            ],
            scope=BotCommandScopeUsers(),
        )
        await self.set_bot_commands(
            [
                BotCommand("all", "Tag all the members"),
                BotCommand("stopall", "Stops a running @all"),
                BotCommand("lottery", "Choose, and tag, one user from the group"),
            ],
            scope=BotCommandScopeChats(),
        )
        await self.set_bot_commands(
            [
                BotCommand("all", "Tag all the members"),
                BotCommand("stopall", "Stops a running @all"),
                BotCommand(
                    "copymessage",
                    "Set the bot to copy the message that contains @all"
                    " instead of replying to it",
                ),
                BotCommand(
                    "nocopymessage",
                    "Set the bot to reply to the message that contains @all.",
                ),
                BotCommand("onlyadmins", "Set the bot to work only for admins"),
                BotCommand("noonlyadmins", "Set the bot to work with everyone"),
            ],
            scope=BotCommandScopeChatAdmins(),
        )

    async def worker(self, chat_id, topic_id):
        try:
            chat = await self.get_entity(chat_id)
        except Exception:
            return
        async with self.pool.acquire() as conn:
            self.logger.info(f"Invio tag per {chat.title} ({chat_id})")
            out = await conn.fetch(
                """SELECT "id", "user_id", "full_name", "reply_to", "message_link", "message_text"
                   FROM "TaggaBots"."to_tag"
                   JOIN "TaggaBots"."users_to_tag" ON "to_tag_id" = "id"
                   WHERE "chat_id" = $1 AND "bot" = $2""",
                chat_id,
                self.username,
            )
        self.logger.info("Invio tag per %s (%s) [%s]", chat.title, chat_id, len(out))
        entities = 0
        users = []
        for (
            to_tag_id,
            user_id,
            full_name,
            reply_to,
            message_link,
            message_text,
        ) in out:
            if message_text:
                reply_to = None
                if topic_id:
                    reply_to = topic_id
            entities += 1
            users.append((user_id, full_name))
            if entities % ENTITIES_PER_MESSAGE == 0 or entities == len(out):
                txt = ""
                if message_text:
                    txt = f"{message_text}\n"
                    if message_link:
                        txt += f'<a href="{message_link}">🔗</a>\n'
                    txt += "\n"
                for user_id, full_name in users:
                    escaped_full_name = html.escape(full_name)
                    txt += f'<a href="tg://user?id={user_id}">{escaped_full_name}</a>, '
                txt = txt[:-2]
                await asyncio.sleep(3)
                try:
                    await self.send_message(
                        chat_id, txt, reply_to=reply_to, parse_mode="HTML"
                    )
                except Exception as e:
                    self.logger.warning(
                        f"Can't send messages to {chat.title} ({chat_id}): {str(e)}"
                    )
                    if isinstance(e, ChatWriteForbiddenError):
                        break
                async with self.pool.acquire() as conn:
                    await conn.execute(
                        """DELETE FROM "TaggaBots"."users_to_tag"
                           WHERE "to_tag_id" = $1 AND "user_id" = ANY($2)""",
                        to_tag_id,
                        [x[0] for x in users],
                    )
                users.clear()
        if out:
            async with self.pool.acquire() as conn:
                await conn.execute(
                    'DELETE FROM "TaggaBots"."to_tag" WHERE "id" = $1', to_tag_id
                )
        self.logger.info(f"Tag finiti per {chat.title} ({chat_id})")

    async def process_queues(self):
        # Cleanup
        async with self.pool.acquire() as conn:
            await conn.execute(
                """DELETE FROM "TaggaBots"."to_tag" WHERE NOT EXISTS
                   (SELECT FROM "TaggaBots"."users_to_tag"
                    WHERE "users_to_tag"."to_tag_id" = "to_tag"."id") AND "bot" = $1""",
                self.username,
            )

            out = await conn.fetch(
                """SELECT "chat_id", "topic_id" FROM "TaggaBots"."to_tag"
                   WHERE "bot" = $1""",
                self.username,
            )
            for row in out:
                chat_id, topic_id = row
                task = self.loop.create_task(
                    self.worker(chat_id, topic_id), name=f"worker{chat_id}_{topic_id}"
                )
                self.worker_tasks.add(task)
                task.add_done_callback(self.worker_tasks.discard)

    async def process_chat(self, to_tag_id, chat):
        chat_id = get_peer_id(chat)
        retries = 10
        while retries > 0:
            try:
                async for user in self.iter_participants(chat):
                    # The user is a bot
                    if user.bot:
                        continue
                    # The user is a deleted account
                    if user.first_name is None:
                        continue
                    async with self.pool.acquire() as conn:
                        out = await conn.fetchval(
                            """SELECT 1 FROM "TaggaBots"."vacations"
                               WHERE "user_id" = $1 AND ("chat_id" IS NULL OR "chat_id" = $2)""",
                            user.id,
                            chat_id,
                        )
                        if not out:
                            await conn.execute(
                                """INSERT INTO "TaggaBots"."users_to_tag" VALUES ($1, $2, $3)
                                   ON CONFLICT DO NOTHING""",
                                to_tag_id,
                                user.id,
                                get_display_name(user),
                            )
            except Exception:
                self.logger.exception("")
                retries -= 1
                await asyncio.sleep(3)
            else:
                break

    # HANDLERS

    async def onlyadmins_hndlr(self, event):
        sender = await event.get_sender()
        lang_code = getattr(sender, "lang_code", None) if sender else None
        chat_id = get_peer_id(await event.get_input_chat())
        if event.sender_id not in await self.get_admin_ids(chat_id):
            return
        async with self.pool.acquire() as conn:
            await conn.execute(
                """INSERT INTO "TaggaBots"."chats"("chat_id", "admin_only") VALUES ($1, TRUE)
                   ON CONFLICT ("chat_id") DO UPDATE SET "admin_only" = TRUE""",
                chat_id,
            )

        if lang_code == "it":
            await event.reply("Il bot da ora accetterà i tag solo dagli admin.")
        else:
            await event.reply("The bot will only accept tags from admins.")
        self.logger.info(f"/onlyadmins {chat_id}")

    async def noonlyadmins_hndlr(self, event):
        sender = await event.get_sender()
        lang_code = getattr(sender, "lang_code", None) if sender else None
        chat_id = get_peer_id(await event.get_input_chat())
        if event.sender_id not in await self.get_admin_ids(chat_id):
            return
        async with self.pool.acquire() as conn:
            await conn.execute(
                """INSERT INTO "TaggaBots"."chats"("chat_id", "admin_only") VALUES ($1, FALSE)
                   ON CONFLICT ("chat_id") DO UPDATE SET "admin_only" = FALSE""",
                chat_id,
            )

        if lang_code == "it":
            await event.reply("Il bot da ora accetterà i tag da tutti.")
        else:
            await event.reply("The bot will accept tags from anybody.")
        self.logger.info(f"/noonlyadmins {chat_id}")

    async def all_hndlr(self, event):
        sender = await event.get_sender()
        lang_code = getattr(sender, "lang_code", None) if sender else None
        chat = await event.get_chat()
        chat_id = get_peer_id(chat)

        topic_id = 0
        if event.reply_to and event.reply_to.forum_topic:
            topic_id = event.reply_to.reply_to_msg_id

        if isinstance(chat, Channel):
            me = await self.get_me(True)
            # If the bot is muted, ignore the command
            participant = await self(GetParticipantRequest(chat, me))
            if isinstance(participant.participant, ChannelParticipantBanned):
                if participant.participant.banned_rights.send_plain:
                    return

            # If participants_hidden=True, send a warning and abort
            permissions = await self.get_permissions(chat, me)
            if not permissions.is_admin:
                ch_full = await self.get_full_entity(chat)
                if ch_full.full_chat.participants_hidden:
                    if lang_code == "it":
                        await event.reply(
                            "Questo guppo ha la lista utenti nascosta.\nIl bot deve"
                            " essere admin per poter accedere alla lista utenti."
                        )
                    else:
                        await event.reply(
                            "This group have the participant list is hidden.\nThe bot"
                            " must be admin to access the participant list."
                        )
                    return

        async with self.locks[chat_id]:
            async with self.pool.acquire() as conn:
                self.logger.info(f"all {chat.title} ({chat_id})")
                row = await conn.fetchrow(
                    """SELECT "admin_only", "copy_message" FROM "TaggaBots"."chats"
                       WHERE "chat_id" = $1""",
                    chat_id,
                )
                if row:
                    admin_only, copy_message = row
                else:
                    admin_only = copy_message = False
                if admin_only and event.sender_id not in await self.get_admin_ids(
                    chat_id
                ):
                    return
                out = await conn.fetchval(
                    'SELECT 1 FROM "TaggaBots"."to_tag" WHERE "chat_id" = $1',
                    chat_id,
                )
            if out:
                if lang_code == "it":
                    await event.reply(
                        "@all già in corso. Questo comando sarà ignorato."
                    )
                else:
                    await event.reply(
                        "@all already is progress. This command will be ignored."
                    )
                self.logger.info(
                    f"all already in progress on {chat.title} ({chat_id}). ignoring"
                )
                return
            else:
                if lang_code == "it":
                    message = await event.reply(
                        "Comando accettato con successo.\nIl bot inizierà a taggare"
                        " appena possibile."
                    )
                else:
                    message = await event.reply(
                        "Command accepted successfully.\nThe bot will start tagging as"
                        " soon as possibile."
                    )

            to_link = (
                chat.username if getattr(chat, "username", None) else f"c/{chat.id}"
            )
            message_link = f"https://t.me/{to_link}/{event.id}"
            if event.reply_to:
                message_link = f"{message_link}?thread={event.reply_to.reply_to_msg_id}"

            async with self.pool.acquire() as conn:
                to_tag_id = await conn.fetchval(
                    '''INSERT INTO "TaggaBots"."to_tag"
                       ("chat_id", "topic_id", "reply_to", "message_link", "sender_id", "bot", "message_text")
                       VALUES ($1, $2, $3, $4, $5, $6, $7) RETURNING "id"''',
                    chat_id,
                    topic_id,
                    event.id,
                    message_link,
                    event.sender_id,
                    self.username,
                    event.text if copy_message else None,
                )
            await self.arq.enqueue_job(
                "TaggaBots.worker",
                self.username,
                chat_id,
                topic_id,
                to_tag_id,
                _job_id=f"TaggaBots.worker:{self.username}:{chat_id}:{topic_id}",
            )
            return
            await self.process_chat(to_tag_id, chat)

            task = self.loop.create_task(
                self.worker(chat_id, topic_id), name=f"worker{chat_id}_{topic_id}"
            )
            self.worker_tasks.add(task)
            task.add_done_callback(self.worker_tasks.discard)
            await message.delete()

    async def stopall_hndlr(self, event):
        sender = await event.get_sender()
        lang_code = getattr(sender, "lang_code", None) if sender else None
        chat = await event.get_input_chat()
        chat_id = get_peer_id(chat)

        topic_id = 0
        if event.reply_to and event.reply_to.forum_topic:
            topic_id = event.reply_to.reply_to_msg_id

        async with self.locks[chat_id]:
            async with self.pool.acquire() as conn:
                sender_id = await conn.fetchval(
                    'SELECT "sender_id" FROM "TaggaBots"."to_tag" WHERE "chat_id" = $1',
                    chat_id,
                )

            if (
                event.sender_id == sender_id
                or event.sender_id in await self.get_admin_ids(chat_id)
            ):
                if sender_id:
                    #                    for task in asyncio.all_tasks():
                    #                        if task.get_name() == f"worker{chat_id}_{topic_id}":
                    #                            task.cancel()
                    await self.arq.enqueue_job(
                        "TaggaBots.abort",
                        self.username,
                        chat_id,
                        topic_id,
                        _job_id=f"TaggaBots.abort:{self.username}:{chat_id}:{topic_id}",
                    )
                    async with self.pool.acquire() as conn:
                        await conn.execute(
                            """DELETE FROM "TaggaBots"."to_tag" WHERE "chat_id" = $1
                               AND "topic_id" = $2 AND "bot" = $3""",
                            chat_id,
                            topic_id,
                            self.username,
                        )
                    if lang_code == "it":
                        await event.reply("@all fermato con successo.")
                    else:
                        await event.reply("@all stopped successfully.")
                else:
                    if lang_code == "it":
                        await event.reply("@all non è attualmente in corso.")
                    else:
                        await event.reply("@all is not running.")

    async def copymessage_hndlr(self, event):
        sender = await event.get_sender()
        lang_code = getattr(sender, "lang_code", None) if sender else None
        chat = await event.get_chat()
        chat_id = get_peer_id(chat)

        if event.sender_id not in await self.get_admin_ids(chat_id):
            return

        async with self.pool.acquire() as conn:
            await conn.execute(
                """INSERT INTO "TaggaBots"."chats"("chat_id", "copy_message")
                   VALUES ($1, TRUE)
                   ON CONFLICT ("chat_id") DO UPDATE SET "copy_message" = TRUE""",
                chat_id,
            )

        if lang_code == "it":
            await event.reply(
                "Il bot da ora copierà il messaggio contenente @all invece di"
                " rispondere ad esso.",
            )
        else:
            await event.reply(
                "The bot will copy the message text instead of replying to it.",
            )
        self.logger.info("/copymessage %s (%s)", chat.title, chat.id)

    async def nocopymessage_hndlr(self, event):
        sender = await event.get_sender()
        lang_code = getattr(sender, "lang_code", None) if sender else None
        chat = await event.get_chat()
        chat_id = get_peer_id(chat)

        if event.sender_id not in await self.get_admin_ids(chat_id):
            return

        async with self.pool.acquire() as conn:
            await conn.execute(
                """INSERT INTO "TaggaBots"."chats"("chat_id", "copy_message")
                   VALUES ($1, TRUE)
                   ON CONFLICT ("chat_id") DO UPDATE SET "copy_message" = FALSE""",
                chat_id,
            )

        if lang_code == "it":
            await event.reply(
                "Il bot da ora risponderà al messaggio contentente @all.",
            )
        else:
            await event.reply(
                "The bot will reply to the message that contains @all.",
            )
        self.logger.info("/copymessage %s (%s)", chat.title, chat.id)

    async def vacation_hndlr(self, event):
        chat_id = None
        if event.pattern_match.group("chat_id"):
            chat_id = int(event.pattern_match.group("chat_id"))
            try:
                chat = await self.get_entity(chat_id)
            except Exception:
                await event.reply(
                    "La chat in questione non è valida.\nVerifica di aver specificato"
                    " la chat_id valida (solitamente inizia con -100)."
                )
                return
        async with self.pool.acquire() as conn:
            if chat_id is None:
                await conn.execute(
                    'DELETE FROM "TaggaBots"."vacations" WHERE "user_id" = $1',
                    event.sender_id,
                )
            await conn.execute(
                """INSERT INTO "TaggaBots"."vacations" ("user_id", "chat_id") VALUES ($1, $2)
                ON CONFLICT DO NOTHING""",
                event.sender_id,
                chat_id,
            )
        if chat_id is None:
            await event.reply(
                "Da ora in poi sei in vacanza, significa che il bot non ti taggherà"
                " più."
            )
        else:
            escaped_chat_title = html.escape(chat.title)
            await event.reply(
                "Da ora in poi sei in vacanza da"
                f" {escaped_chat_title} [<code>{chat_id}</code>], significa che il bot"
                " non ti taggherà più in quel gruppo.",
                parse_mode="HTML",
            )

    async def stopvacation_hndlr(self, event):
        chat_id = None
        if event.pattern_match.group("chat_id"):
            chat_id = int(event.pattern_match.group("chat_id"))
            try:
                chat = await self.get_entity(chat_id)
            except Exception:
                await event.reply(
                    "La chat in questione non è valida.\nVerifica di aver specificato"
                    " la chat_id valida (solitamente inizia con -100)."
                )
                return
        if chat_id:
            async with self.pool.acquire() as conn:
                out = await conn.execute(
                    """DELETE FROM "TaggaBots"."vacations"
                       WHERE "user_id" = $1 AND "chat_id" = $2""",
                    event.sender_id,
                    chat_id,
                )
            if int(out.split(" ")[-1]):
                escaped_chat_title = html.escape(chat.title)
                await event.reply(
                    "D'ora in poi non sei più in vacanza su"
                    f" {escaped_chat_title} [<code>{chat_id}</code>], significa che il"
                    " bot può taggarti lì.",
                    parse_mode="HTML",
                )
            else:
                await event.reply("Non eri in vacanza o la chat non è valida.")
        else:
            async with self.pool.acquire() as conn:
                out = await conn.execute(
                    'DELETE FROM "TaggaBots"."vacations" WHERE "user_id" = $1',
                    event.sender_id,
                )
            if int(out.split(" ")[-1]):
                await event.reply(
                    "D'ora in poi non sei più in vacanza, significa che il bot può"
                    " taggarti ovunque."
                )
            else:
                await event.reply("Non eri in vacanza da nessuna parte.")

    async def vacations_hndlr(self, event):
        async with self.pool.acquire() as conn:
            out = await conn.fetch(
                """SELECT "chat_id" FROM "TaggaBots"."vacations"
                   WHERE "user_id" = $1""",
                event.sender_id,
            )
        if out:
            txt = "Sei in vacanza da:\n"
            for row in out:
                chat_id = row[0]
                if chat_id is None:
                    txt = "Sei in vacanza da tutti i gruppi."
                    break
                try:
                    chat = await self.get_entity(chat_id)
                except Exception:
                    escaped_chat_title = ""
                else:
                    escaped_chat_title = html.escape(chat.title)
                txt += f" {escaped_chat_title} [<code>{chat_id}</code>]\n"
        else:
            txt = "Non sei in vacanza da nessuna parte."
        await event.reply(txt, parse_mode="HTML")

    async def lottery_hndlr(self, event):
        chat = await event.get_chat()

        users = await self.get_participants(chat)

        while True:
            user = random.choice(users)
            if user.bot:
                continue
            if user.username or user.first_name:
                break

        if user.username:
            user_link = f"@{user.username}"
        else:
            user_link = get_display_name(user)
        user_link = html.escape(user_link)

        await event.reply(
            f"Il fortunato estratto è... {user_link} [<code>{user.id}</code>]",
            parse_mode="HTML",
        )

    async def start_hndlr(self, event):
        sender = await event.get_sender()
        lang_code = getattr(sender, "lang_code", None) if sender else None

        if lang_code == "it":
            await event.reply(
                f"Ciao e benvenuto su @{self.username}.\nQuesto bot serve per far"
                " <b>taggare</b> tutti i membri di un gruppo.\n\nPer utilizzarlo è"
                " sufficente inserirlo in un gruppo (come utente normale) e iniziare"
                " una frase con @all o #all, a questo punto il bot inizierà a"
                f" rispondere a quel messaggio taggando {ENTITIES_PER_MESSAGE} persone"
                " alla volta (per via di una limitazione di Telegram).\n\nComandi"
                " utili (da dare in gruppo):\n/stopall - Questo comando ferma un @all"
                " in corso. Il comando può darlo solo chi ha dato @all originariamente"
                " o un admin del gruppo.\n/onlyadmins - Il bot viene impostato per"
                " accettare @all solo dagli admin.\n/noonlyadmins - Il bot viene"
                " impostato per accettare @all da tutti gli utenti.\nSuggerisco di"
                " clonare il bot per avere performance migliori.\nPer farlo, scrivi"
                " /clone in una chat privata con questo bot.\n\n<a"
                ' href="https://gitlab.com/supermariobots/TaggaBots">📄 Codice'
                " sorgente di questo bot 📄</a>",
                parse_mode="HTML",
            )
        else:
            await event.reply(
                f"Hi and welcome to @{self.username}.\nThis bot is used to <b>tag</b>"
                " all the users in a group.\n\nTo use it, it's only needed to insert"
                " it in a group (as standard user) and to start a phrase with @all or"
                " #all, at this point the bot will start to reply to this message by"
                f" tagging {ENTITIES_PER_MESSAGE} users each time(for a Telegram"
                " limitation).\n\nCommands (to write in group):\n/stopall - Stops a"
                " running @all. This command is only avaiable to whom started @all or"
                " to an admin of the group.\n/onlyadmins - Set the bot to works only"
                " for admins.\n/noonlyadmins - Set the bot to work with everyone.\nI"
                " suggest to clone the bot in order to have a better performance.\nIf"
                " you want to do that, just write /clone in a private chat with this"
                ' bot.\n\n<a href="https://gitlab.com/supermariobots/TaggaBots">📄'
                " Source code of this bot 📄</a>",
                parse_mode="HTML",
            )

    async def deleted_hndlr(self, event):
        chat = await event.get_input_chat()
        if chat is None:
            return

        chat_id = get_peer_id(chat)

        async with self.pool.acquire() as conn:
            rows = await conn.fetch(
                """SELECT "id", "topic_id" FROM "TaggaBots"."to_tag"
                   WHERE "chat_id" = $1 AND "reply_to" = ANY($2)""",
                chat_id,
                event.deleted_ids,
            )
        for row in rows:
            to_tag_id, topic_id = row
            for task in asyncio.all_tasks():
                if task.get_name() == f"worker{chat_id}_{topic_id}":
                    task.cancel()
            await self.send_message(
                chat,
                "@all terminato in quanto il messaggio originale è stato cancellato.\n"
                "@all stopped since the original message was deleted.",
                reply_to=topic_id,
            )
            async with self.pool.acquire() as conn:
                await conn.execute(
                    'DELETE FROM "TaggaBots"."to_tag" WHERE "id" = $1',
                    to_tag_id,
                )

    async def _clone_hndlr(self, event):
        sender = await event.get_sender()
        lang_code = getattr(sender, "lang_code", None) if sender else None

        if lang_code == "it":
            await event.reply(
                "<b>Clona questo bot</b>\n"
                "I cloni sono copie identiche di questo bot.\n\n"
                "<b>Per creare un clone:</b>\n"
                " • Vai su @BotFather\n"
                " • Lancia /newbot\n"
                " • Digita il nome che vuoi dare al bot\n"
                " • Digita lo username che vuoi dare al bot\n"
                " • Inoltra qui (su questo bot) il messaggio che ricevi da @BotFather\n"
                " • Fatto\n\n"
                "Per fare in modo che il bot funzioni senza dargli permesso di admin"
                " devi disabilitare la privacy in @BotFather usando /setprivacy, "
                " selezionando il tuo bot e cliccando su Disable.\n"
                "Questo è necessario in quanto il bot ha bisogno di poter accedere a"
                " tutti i messaggi del gruppo per poter trovare i tag @all e #all.",
                parse_mode="HTML",
            )
        else:
            await event.reply(
                "<b>Clone this bot</b>\n"
                "Clones are identically copies of this bot.\n\n"
                "<b>To create a clone:</b>\n"
                " • Go to @BotFather\n"
                " • Launch /newbot\n"
                " • Write the name you want to give to the bot\n"
                " • Write the username you want to give to the bot\n"
                " • Forward the message you receive from@BotFather here\n"
                " • Done\n\n"
                "To make the bot work without giving admin permissions, you need to"
                " disable privacy in @BotFather by using /setprivacy, selecting you"
                " bot and clicking to Disable.\n"
                "This is needed since the bot needs permission to read all the"
                " messages in order to get @all or #all tags.",
                parse_mode="HTML",
            )


# Setup and run
if __name__ == "__main__":
    main(TaggaBots)
